from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import Project_Form


# Create your views here.
@login_required
def list_projects(request):
    projects_list = Project.objects.filter(owner=request.user)
    context = {
        "list_projects": projects_list,
    }
    return render(request, "projects/projectlist.html", context)


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    context = {"show_project": project}
    return render(request, "projects/projectdetail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = Project_Form(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = Project_Form()
    context = {
        "form": form,
    }
    return render(request, "projects/createproject.html", context)
